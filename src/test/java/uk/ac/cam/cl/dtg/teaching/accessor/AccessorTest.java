/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertThrows;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.DeclaresF;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.PackageUnseenClassWithDefaultMethodWrapper;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.PrimitiveConstructor;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.PrivateConstructor;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClass;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWithDefaultMethod;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWithDefaultMethodWrapper;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWithUnseenArgument;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWithUnseenArgumentWrapper;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWithUnseenReturn;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWithUnseenReturnWrapper;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.UnseenClassWrapper;
import uk.ac.cam.cl.dtg.teaching.accessor.testclasses.WithDefaultMethodF;

@RunWith(JUnit4.class)
public class AccessorTest {

  enum Inner {}

  @Test
  public void resolveClassName_findsExactMatch() {
    // ARRANGE
    String className = "java.lang.String";
    Accessor accessor = new Accessor(new NoOpListener());

    // ACT
    String resolvedName = accessor.resolveClassName(className);

    // ASSERT
    assertThat(resolvedName).isEqualTo(className);
  }

  @Test
  public void loadClass_findsRegexMatch_withNestedClass() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());
    String className = "uk.ac.cam.cl.dtg.teaching.*.AccessorTest\\$Inner";

    // ACT
    String resolvedName = accessor.resolveClassName(className);

    // ASSERT
    assertThat(resolvedName).isEqualTo(Inner.class.getName());
  }

  @Test
  public void loadClass_findsRegexMatch() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());
    String className = "uk.ac.cam.cl.dtg.teaching.*.AccessorTest";

    // ACT
    String resolvedName = accessor.resolveClassName(className);

    // ASSERT
    assertThat(resolvedName).isEqualTo(getClass().getName());
  }

  @Test
  public void loadClass_throws_withAmbiguousMatch() {
    // ARRANGE
    String className = getClass().getPackageName() + ".*";
    Accessor accessor = new Accessor(new NoOpListener());

    // ACT + ASSERT
    assertThrows(AccessorException.class, () -> accessor.resolveClassName(className));
  }

  @Test
  public void construct_findsNoArgConstructor_whenPrivate() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());

    // ACT
    Object constructed = accessor.construct("o", PrivateConstructor.class.getCanonicalName());

    // ASSERT
    assertThat(constructed).isNotNull();
  }

  @Test
  public void construct_findsConstructorTakingInt_whenPassedInteger() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());

    // ACT
    Object constructed =
        accessor.construct("o", PrimitiveConstructor.class.getCanonicalName(), Integer.valueOf(1));

    // ASSERT
    assertThat(constructed).isNotNull();
  }

  @Test
  public void construct_findsConstructorTakingInteger_whenPassedInt() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());

    // ACT
    Object constructed = accessor.construct("o", PrimitiveConstructor.class.getCanonicalName(), 1);

    // ASSERT
    assertThat(constructed).isNotNull();
  }

  @Test
  public void invoke_findsDefaultMethod() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());
    WithDefaultMethodF m = new WithDefaultMethodF() {};

    // ACT
    int r = accessor.invoke(m, "f");

    // ASSERT
    assertThat(r).isEqualTo(4);
  }

  @Test
  public void invoke_findsInheritedMethod() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());
    DeclaresF o = new DeclaresF() {};

    // ACT
    int r = accessor.invoke(o, "f");

    // ASSERT
    assertThat(r).isEqualTo(5);
  }

  @Test
  public void invoke_findsOverriddenMethod() {
    // ARRANGE
    Accessor accessor = new Accessor(new NoOpListener());
    DeclaresF o =
        new DeclaresF() {
          @Override
          public int f() {
            return 6;
          }
        };

    // ACT
    int r = accessor.invoke(o, "f");

    // ASSERT
    assertThat(r).isEqualTo(6);
  }

  @Test
  public void constructProxy_wrapsObject() {
    // ARRANGE
    Accessor a = new Accessor(new NoOpListener());
    UnseenClassWrapper unseenObject =
        a.construct("unseen", UnseenClassWrapper.class, UnseenClass.class.getCanonicalName());

    // ACT
    int r = unseenObject.g();

    // ASSERT
    assertThat(r).isEqualTo(6);
  }

  @Test
  public void constructProxy_wrapsObject_withDefaultMethod() {
    // ARRANGE
    Accessor a = new Accessor(new NoOpListener());
    UnseenClassWithDefaultMethodWrapper unseenObject =
        a.construct(
            "unseen",
            UnseenClassWithDefaultMethodWrapper.class,
            UnseenClassWithDefaultMethod.class.getCanonicalName());

    // ACT
    int r = unseenObject.f();

    // ASSERT
    assertThat(r).isEqualTo(4);
  }

  @Test
  public void constructProxy_wrapsReturnedObjects() {
    // ARRANGE
    Accessor a = new Accessor(new NoOpListener());
    UnseenClassWithUnseenReturnWrapper unseenObject =
        a.construct(
            "unseen",
            UnseenClassWithUnseenReturnWrapper.class,
            UnseenClassWithUnseenReturn.class.getCanonicalName());

    // ACT
    int r = unseenObject.f().g();

    // ASSERT
    assertThat(r).isEqualTo(7);
  }

  @Test
  public void constructProxy_wrapsObject_takingItselfAsArgument() {
    // ARRANGE
    Accessor a = new Accessor(new NoOpListener());
    UnseenClassWithUnseenArgumentWrapper unseenObject =
        a.construct(
            "unseen",
            UnseenClassWithUnseenArgumentWrapper.class,
            UnseenClassWithUnseenArgument.class.getCanonicalName());

    // ACT
    int x = unseenObject.f(unseenObject);

    // ASSERT
    assertThat(x).isEqualTo(7);
  }

  @Test
  public void constructProxy_wrapsObject_takingADifferentInstanceOfItselfAsArgument() {
    // ARRANGE
    Accessor a = new Accessor(new NoOpListener());
    UnseenClassWithUnseenArgumentWrapper unseenObject =
        a.construct(
            "unseen",
            UnseenClassWithUnseenArgumentWrapper.class,
            UnseenClassWithUnseenArgument.class.getCanonicalName());
    UnseenClassWithUnseenArgumentWrapper unseenObject2 =
        a.construct(
            "unseen",
            UnseenClassWithUnseenArgumentWrapper.class,
            UnseenClassWithUnseenArgument.class.getCanonicalName());

    // ACT
    int x = unseenObject.f(unseenObject2);

    // ASSERT
    assertThat(x).isEqualTo(7);
  }

  @Test
  public void invoke_succeeds_withDefaultMethod() {
    // ARRANGE
    Accessor a = new Accessor(new NoOpListener());
    PackageUnseenClassWithDefaultMethodWrapper o =
        a.construct(
            "unseen",
            PackageUnseenClassWithDefaultMethodWrapper.class,
            "uk.ac.cam.cl.dtg.teaching.accessor.testclasses.PackageUnseenClassWithDefaultMethod");

    // ACT
    int x = o.f(4);

    // ASSERT
    assertThat(x).isEqualTo(4);
  }
}

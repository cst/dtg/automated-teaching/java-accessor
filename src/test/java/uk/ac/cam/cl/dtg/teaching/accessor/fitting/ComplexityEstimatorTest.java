/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor.fitting;

import static com.google.common.truth.Truth.assertThat;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Function;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ComplexityEstimatorTest {

  @Test
  public void estimate_picksLinear() {
    // ARRANGE
    double[] x = ascending();
    double[] y = noisyMap(x, v -> 4 * v + 5);

    // ACT
    Complexity complexity =
        ComplexityEstimator.estimate(x, y, Complexity.values());

    // ASSERT
    assertThat(complexity).isEqualTo(Complexity.LINEAR);
  }

  @Test
  public void estimate_picksQuadratic() {
    // ARRANGE
    double[] x = ascending();
    double[] y = noisyMap(x, v -> 40 * v * v + 5);

    // ACT
    Complexity complexity =
        ComplexityEstimator.estimate(x, y, Complexity.values());

    // ASSERT
    assertThat(complexity).isEqualTo(Complexity.QUADRATIC);
  }

  @Test
  public void estimate_picksNLogN() {
    // ARRANGE
    double[] x = ascending();
    double[] y = noisyMap(x, v -> 40 * v * Math.log(v) + 5);

    // ACT
    Complexity complexity =
        ComplexityEstimator.estimate(x, y, Complexity.values());

    // ASSERT
    assertThat(complexity).isEqualTo(Complexity.NLOGN);
  }

  @Test
  public void estimate_picksExponential() {
    // ARRANGE
    double[] x = ascending();
    double[] y = noisyMap(x, v -> Math.pow(3, v) + 5);

    // ACT
    Complexity complexity =
        ComplexityEstimator.estimate(x, y, Complexity.values());

    // ASSERT
    assertThat(complexity).isEqualTo(Complexity.EXPONENTIAL);
  }

  private static double[] noisyMap(double[] x, Function<Double, Double> mapper) {
    Random r = new Random(23521);
    return Arrays.stream(x).map(mapper::apply).map(v -> v + r.nextGaussian() * 10).toArray();
  }

  private static double[] ascending() {
    double[] x = new double[10];
    for (int i = 0; i < x.length; i++) {
      x[i] = i + 1;
    }
    return x;
  }
}

/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.ac.cam.cl.dtg.teaching.accessor.testing.ParameterisedTest;
import uk.ac.cam.cl.dtg.teaching.accessor.testing.SampleTestWithPurpose;
import uk.ac.cam.cl.dtg.teaching.accessor.testing.SampleTestWithoutPurpose;

@RunWith(JUnit4.class)
public class JUnitWrapperTest {

  private static class FakeListener extends NoOpListener {
    private String startingTestTestName;
    private String startingTestTestPurpose;

    @Override
    public void startingTest(String testName, String testPurpose) {
      startingTestTestName = testName;
      startingTestTestPurpose = testPurpose;
    }
  }

  @Test
  public void startingTest_collectsPurpose_whenAnnotated() {
    // ARRANGE
    FakeListener listener = new FakeListener();

    // ACT
    JUnitWrapper.runTests(listener, SampleTestWithPurpose.class);

    // ASSERT
    assertThat(listener.startingTestTestName).isEqualTo("sampleTest");
    assertThat(listener.startingTestTestPurpose).isEqualTo("Check whether the sample test works");
  }

  @Test
  public void startingTest_collectsNoPurpose_whenNotAnnotated() {
    // ARRANGE
    FakeListener listener = new FakeListener();

    // ACT
    JUnitWrapper.runTests(listener, SampleTestWithoutPurpose.class);

    // ASSERT
    assertThat(listener.startingTestTestName).isEqualTo("sampleTest");
    assertThat(listener.startingTestTestPurpose).isEmpty();
  }

  @Test
    public void startingTest_collectsName_onParameterisedTest() {
      // ARRANGE
      FakeListener listener = new FakeListener();

      // ACT
      JUnitWrapper.runTests(listener, ParameterisedTest.class);

      // ASSERT
      assertThat(listener.startingTestTestName).isEqualTo("test[1]");
  }
}

/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor;

import com.google.common.collect.Iterables;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runners.JUnit4;
import org.junit.runners.Parameterized;

public class JUnitWrapper {

  static TestActionListener testActionListener;

  public static void runTestsAndExitVm(
      TestActionListener testActionListener, Class<?>... testClasses) {
    Result r = runTests(testActionListener, testClasses);
    System.exit(r.wasSuccessful() ? 0 : -1);
  }

  static Result runTests(TestActionListener testActionListener, Class<?>... testClasses) {
    JUnitWrapper.testActionListener = testActionListener;
    JUnitCore junitCore = new JUnitCore();
    junitCore.addListener(
        new RunListener() {
          @Override
          public void testRunFinished(Result result) {
            int passed = result.getRunCount() - result.getFailureCount();
            testActionListener.testsComplete(passed, result.getRunCount());
          }

          @Override
          public void testStarted(Description description) {
            Purpose p = description.getAnnotation(Purpose.class);
            String purpose = p != null ? p.value() : "";
            testActionListener.startingTest(getTestName(description), purpose);
          }

          @Override
          public void testFailure(Failure failure) {
            Throwable exception = failure.getException();
            while ((exception instanceof InvocationTargetException
                    || exception instanceof AccessorException)
                && exception.getCause() != null) {
              exception = exception.getCause();
            }
            testActionListener.testFailure(exception.toString());
          }
        });
    return junitCore.run(testClasses);
  }

  private static final Pattern PARAMETERISED_DESCRIPTION = Pattern.compile("\\[(\\d+)]$");

  private static String getTestName(Description description) {
    Class<?> testClass = description.getTestClass();
    RunWith runWith = testClass.getAnnotation(RunWith.class);
    if (runWith == null || runWith.value().equals(JUnit4.class)) {
      return description.getMethodName();
    }
    if (runWith.value().equals(Parameterized.class)) {
      Matcher m = PARAMETERISED_DESCRIPTION.matcher(description.getMethodName());
      if (!m.find()) {
        throw new IllegalArgumentException(
            "Failed to parse method description " + description.getMethodName());
      }
      int parameterIndex = Integer.parseInt(m.group(1));
      String parameterisationName = getParameterisationName(testClass, parameterIndex);
      return m.replaceAll("[" + parameterisationName + "]");
    }
    throw new IllegalArgumentException("Unrecognised test runner " + runWith.value());
  }

  private static String getParameterisationName(Class<?> testClass, int parameterisationIndex) {
    Collection<Object[]> data = getParameterisationData(testClass);
    Object[] current = Iterables.get(data, parameterisationIndex);
    return (String) current[0];
  }

  private static Collection<Object[]> getParameterisationData(Class<?> testClass) {
    for (Method m : testClass.getDeclaredMethods()) {
      if (Modifier.isStatic(m.getModifiers())
          && m.getAnnotation(Parameterized.Parameters.class) != null
          && m.getReturnType().equals(Collection.class)) {
        try {
          //noinspection unchecked
          return (Collection<Object[]>) m.invoke(null);
        } catch (IllegalAccessException | InvocationTargetException e) {
          throw new IllegalArgumentException(
              "Failed to invoke Parameters method " + m.getName() + " on " + testClass.getName());
        }
      }
    }
    throw new IllegalArgumentException(
        "Failed to find a @Parameters method in " + testClass.getName());
  }
}

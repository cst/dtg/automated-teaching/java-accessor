/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor.fitting;

import java.util.function.BiFunction;

public enum Complexity {
  LINEAR((xs, ys) -> FittedPolynomial.create(xs, ys, 1), "O(N)"),
  QUADRATIC((xs, ys) -> FittedPolynomial.create(xs, ys, 2), "O(N^2)"),
  NLOGN(FittedNLogN::create, "O(NLOGN)"),
  EXPONENTIAL(FittedExponential::create, "O(2^N)");

  private final BiFunction<double[], double[], FittedCurve> fitter;
  private final String name;

  Complexity(BiFunction<double[], double[], FittedCurve> fitter, String name) {
    this.fitter = fitter;
    this.name = name;
  }

  FittedCurve fit(double[] x, double[] y) {
    return fitter.apply(x, y);
  }

  @Override
  public String toString() {
    return name;
  }
}

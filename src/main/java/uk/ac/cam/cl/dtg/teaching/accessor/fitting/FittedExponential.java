/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor.fitting;

import com.google.common.collect.ImmutableList;

class FittedExponential extends FittedCurve {

  static FittedExponential create(double[] x, double[] y) {
    FittedExponential f = new FittedExponential();
    f.fit(x, y);
    return f;
  }

  @Override
  Complexity complexity() {
    return Complexity.EXPONENTIAL;
  }

  @Override
  double unmapY(double y) {
    return Math.pow(10, y);
  }

  @Override
  ImmutableList<Point> mapValues(double[] x, double[] y) {
    ImmutableList.Builder<Point> result = ImmutableList.builder();
    for (int i = 0; i < x.length; ++i) {
      Point p = new Point(x[i], Math.log(y[i]));
      result.add(p);
    }
    return result.build();
  }
}

/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Streams;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.stream.Collectors;

public class ConsoleLoggingListener implements TestActionListener {

  private static final ImmutableSet<Class<?>> TO_STRING_CLASSES =
      ImmutableSet.of(
          Integer.class,
          Double.class,
          Float.class,
          String.class,
          Boolean.class,
          Double.class,
          Short.class,
          Character.class);
  private final IdentityHashMap<Object, String> objectNames;
  private final PrintStream out;
  private boolean muted;

  public ConsoleLoggingListener() {
    this(System.out);
  }

  private ConsoleLoggingListener(PrintStream out) {
    this.objectNames = new IdentityHashMap<>();
    this.out = out;
    this.muted = false;
  }

  @Override
  public void startingTest(String testName, String testPurpose) {
    muted = false;
    if (testPurpose.isEmpty()) {
      printf("%n- Running test %s%n", testName);
    } else {
      printf("%n- Running test %s: %s%n", testName, testPurpose);
    }
  }

  @Override
  public void testFailure(String failureMessage) {
    muted = false;
    printf("- Test failed: %s%n", failureMessage);
  }

  @Override
  public void testsComplete(int totalPassed, int totalExecuted) {
    muted = false;
    printf(
        "%n%s: %d out of %d tests passed%n",
        totalPassed == totalExecuted ? "Pass" : "Fail", totalPassed, totalExecuted);
  }

  @Override
  public void constructingClass(String className, Iterable<Object> params) {
    printf(
        "- Creating new object using constructor %s(%s)%n",
        className, stringRepresentation(params));
  }

  @Override
  public void createdObject(String instanceName, Object instance) {
    objectNames.put(instance, instanceName);
    printf("- New object will be referred to as '%s'%n", instanceName);
  }

  @Override
  public void gettingStaticField(String className, String fieldName) {
    printf("- Getting static field %s.%s%n", className, fieldName);
  }

  @Override
  public void gettingInstanceField(Object instance, String fieldName) {
    printf("- Getting instance field %s.%s%n", stringRepresentation(instance), fieldName);
  }

  @Override
  public void invokingStaticMethod(String className, String methodName, Iterable<Object> params) {
    printf(
        "- Invoking static method %s.%s(%s)%n",
        className, methodName, stringRepresentation(params));
  }

  @Override
  public void invokingInstanceMethod(Object instance, String methodName, Iterable<Object> params) {
    printf(
        "- Invoking instance method %s.%s(%s)%n",
        stringRepresentation(instance), methodName, stringRepresentation(params));
  }

  @Override
  public void checkingForDeclaredMethod(
      String className, String methodName, Iterable<Object> parameterTypes) {
    printf(
        "- Checking for method declared in class %s.%s(%s)%n",
        className, methodName, stringRepresentation(parameterTypes));
  }

  @Override
  public void checkingForSuperClass(String className, String superClassPattern) {
    printf("- Checking that name of superclass of %s matches %s%n", className, superClassPattern);
  }

  @Override
  public void mute(String summary) {
    printf("- %s%n", summary);
    muted = true;
  }

  @Override
  public void unmute() {
    muted = false;
  }

  @Override
  public void log(String message) {
    printf("- %s%n", message);
  }

  private void printf(String message, Object... params) {
    if (muted) {
      return;
    }
    out.printf(message, params);
  }

  private String stringRepresentation(Iterable<Object> objects) {
    return Streams.stream(objects).map(this::stringRepresentation).collect(Collectors.joining(","));
  }

  private String stringRepresentation(Object o) {
    if (o == null) {
      return "<null>";
    }
    if (objectNames.containsKey(o)) {
      return objectNames.get(o);
    }
    if (o.getClass().isArray()) {
      Class<?> componentType = o.getClass().getComponentType();
      if (componentType == Integer.TYPE) {
        return Arrays.toString((int[]) o);
      }
      if (componentType == Byte.TYPE) {
        return Arrays.toString((byte[]) o);
      }
      if (componentType == Long.TYPE) {
        return Arrays.toString((long[]) o);
      }
      if (componentType == Boolean.TYPE) {
        return Arrays.toString((boolean[]) o);
      }
      if (componentType == Short.TYPE) {
        return Arrays.toString((short[]) o);
      }
      if (componentType == Character.TYPE) {
        return Arrays.toString((char[]) o);
      }
      if (componentType == Double.TYPE) {
        return Arrays.toString((double[]) o);
      }
      if (componentType == Float.TYPE) {
        return Arrays.toString((float[]) o);
      }
      if (componentType.isArray() && componentType.getComponentType() == Double.TYPE) {
        return Arrays.deepToString((double[][]) o);
      }

      return Arrays.toString((Object[]) o);
    }
    if (TO_STRING_CLASSES.contains(o.getClass())) {
      return o.toString();
    }
    if (o.getClass().getAnnotation(ToStringRepresentation.class) != null) {
      return o.toString();
    }
    return o.getClass().getName();
  }
}

/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor.fitting;

import static com.google.common.collect.ImmutableList.toImmutableList;

import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.Comparator;

public class ComplexityEstimator {

  public static Complexity estimate(double[] x, double[] y, Complexity... choices) {
    ImmutableList<FittedCurve> sorted =
        Arrays.stream(choices)
            .map(c -> c.fit(x, y))
            .sorted(Comparator.comparing(FittedCurve::holdOutError))
            .collect(toImmutableList());
    return sorted.stream().map(FittedCurve::complexity).findFirst().orElseThrow();
  }

  public static Complexity estimate(int[] x, int[] y, Complexity... choices) {
    return estimate(
        Arrays.stream(x).mapToDouble(v -> v).toArray(),
        Arrays.stream(y).mapToDouble(v -> v).toArray(),
        choices);
  }

  private ComplexityEstimator() {}
}

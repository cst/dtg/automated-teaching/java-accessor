/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.ac.cam.cl.dtg.teaching.accessor;

import static com.google.common.collect.ImmutableList.toImmutableList;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

public class Accessor {

  private final TestActionListener actionListener;

  Accessor(TestActionListener actionListener) {
    this.actionListener = actionListener;
  }

  public <T> T construct(
      String instanceName, Class<T> targetInterface, String className, Object... params) {
    Object instance = construct(instanceName, className, params);
    return wrap(targetInterface, instance);
  }

  /**
   * Create a new instance of the class given (from searching the classpath).
   *
   * @param instanceName the symbolic name for referring to instances of this object later
   * @param className the fully-qualified name of the class to construct, can be a regular
   *     expression
   * @param params parameters to pass to the constructor
   * @return an instance of the new class
   */
  public Object construct(String instanceName, String className, Object... params) {
    List<Object> parameters = params == null ? List.of() : Arrays.asList(params);
    String realName = resolveClassName(className);
    actionListener.constructingClass(realName, parameters);
    Class<?> clazz = loadClass(realName);
    Constructor<?> c = findConstructor(clazz, parameters);
    try {
      Object result = c.newInstance(params);
      actionListener.createdObject(instanceName, result);
      return result;
    } catch (ReflectiveOperationException e) {
      throw new AccessorException(e);
    }
  }

  /**
   * Get the specified field from this object (overriding access controls).
   *
   * @param <T> the type of the field to access
   * @param instance the instance of the object or the name (can be a regexp) of the class for
   *     static fields
   * @param field the name of the field
   * @return the value held in the field
   */
  public <T> T getField(Object instance, String field) {
    Class<?> c;
    if (instance instanceof String) {
      String className = resolveClassName((String) instance);
      actionListener.gettingStaticField(className, field);
      c = loadClass(className);
      instance = null;
    } else {
      c = instance.getClass();
      actionListener.gettingInstanceField(instance, field);
    }

    Field f = findField(c, field);
    try {
      @SuppressWarnings("unchecked")
      T r = (T) f.get(instance);
      return r;
    } catch (ReflectiveOperationException e) {
      throw new AccessorException(e);
    }
  }

  /**
   * Invoke the specified method (overriding access controls).
   *
   * @param <T> the return type of the method
   * @param o the object to invoke the method upon or the name (can be a regexp) of the class for
   *     static methods.
   * @param methodName the name of the method to invoke
   * @param params arguments to pass to the method
   * @return the value returned by the method
   */
  public <T> T invoke(Object o, String methodName, Object... params) {
    return invoke(o, methodName, params == null ? List.of() : Arrays.asList(params));
  }

  public <T> T invoke(Object o, String methodName, List<Object> params) {
    return methodInvoker(o, methodName, params);
  }

  /**
   * Invoke the main method on this class.
   *
   * @param className the name of the class (can be a regexp)
   * @param params parameters to pass to the main method
   * @return the result of the main method
   */
  public Object invokeMain(String className, String... params) {
    if (params == null) {
      params = new String[] {};
    }
    return invoke(className, "main", new Object[] {params});
  }

  private <T> T methodInvoker(Object o, String methodName, List<Object> parameters) {

    Class<?> c;
    if (o instanceof String) { // this is a class name
      String className = resolveClassName((String) o);
      actionListener.invokingStaticMethod(className, methodName, parameters);
      c = loadClass(className);
      o = null;
    } else {
      c = o.getClass();
      actionListener.invokingInstanceMethod(o, methodName, parameters);
    }

    Method m = findMethod(c, methodName, parameters);
    if (o == null && !Modifier.isStatic(m.getModifiers())) {
      throw new AccessorException("Method " + m + " is not static");
    }
    try {
      @SuppressWarnings("unchecked")
      T res = (T) m.invoke(o, parameters.toArray());
      return res;
    } catch (ReflectiveOperationException e) {
      throw new AccessorException(e);
    }
  }

  public Class<?> findClass(String className) {
    return loadClass(resolveClassName(className));
  }

  public boolean declaresMethod(
      String classNamePattern, String methodName, Class<?>... parameterTypes) {
    String name = resolveClassName(classNamePattern);
    //noinspection RedundantCast
    actionListener.checkingForDeclaredMethod(
        name, methodName, Arrays.asList((Object[]) parameterTypes));
    Class<?> clazz = loadClass(name);
    try {
      clazz.getDeclaredMethod(methodName, parameterTypes);
      return true;
    } catch (NoSuchMethodException e) {
      return false;
    }
  }

  public boolean declaresAbstractMethod(
      String classNamePattern, String methodName, Class<?>... parameterTypes) {
    String name = resolveClassName(classNamePattern);
    //noinspection RedundantCast
    actionListener.checkingForDeclaredMethod(
        name, methodName, Arrays.asList((Object[]) parameterTypes));
    Class<?> clazz = loadClass(name);
    try {
      Method m = clazz.getDeclaredMethod(methodName, parameterTypes);
      return Modifier.isAbstract(m.getModifiers());
    } catch (NoSuchMethodException e) {
      return false;
    }
  }

  public boolean extendsClass(String classNamePattern, String superNamePattern) {
    String name = resolveClassName(classNamePattern);
    actionListener.checkingForSuperClass(name, classNamePattern);
    Class<?> clazz = loadClass(name);
    String superName = clazz.getSuperclass().getName();
    return superName.matches(superNamePattern);
  }

  public ImmutableSet<String> ancestors(String classNamePattern) {
    String name = resolveClassName(classNamePattern);
    Class<?> clazz = loadClass(name);
    return ancestors(clazz);
  }

  public ImmutableSet<String> ancestors(Class<?> clazz) {
    ImmutableSet.Builder<String> result = ImmutableSet.builder();
    result.add(clazz.getName());
    Class<?> superClass = clazz.getSuperclass();
    if (superClass != null) {
      result.addAll(ancestors(superClass));
    }
    for (Class<?> i : clazz.getInterfaces()) {
      result.addAll(ancestors(i));
    }
    return result.build();
  }

  String resolveClassName(String className) {
    Pattern innerClass = Pattern.compile("(.*)\\\\(\\$.+)");
    String topLevelName = className;
    String innerClassName = "";
    Matcher m = innerClass.matcher(className);
    if (m.matches()) {
      topLevelName = m.group(1);
      innerClassName = m.group(2);
    }
    Pattern p = Pattern.compile(topLevelName);
    try {
      ImmutableList<String> names =
          ClassPath.from(getClass().getClassLoader()).getTopLevelClasses().stream()
              .map(ClassPath.ClassInfo::getName)
              .filter(p.asMatchPredicate())
              .collect(toImmutableList());
      if (names.isEmpty()) {
        return className;
      }
      if (names.size() > 1) {
        throw new AccessorException(
            "Ambiguous class name " + className + " could match " + String.join(",", names));
      }
      return names.get(0) + innerClassName;
    } catch (IOException e) {
      throw new AccessorException("Failed to scan for matching class names", e);
    }
  }

  interface InstanceReturner {

    Object __internalGetWrappedInstance();
  }

  private <T> T wrap(Class<T> targetInterface, Object instance) {
    return targetInterface.cast(
        Proxy.newProxyInstance(
            targetInterface.getClassLoader(),
            new Class<?>[] {targetInterface, InstanceReturner.class},
            (proxy, method, args) -> {
              if (method.getName().equals("__internalGetWrappedInstance")) {
                return instance;
              }
              List<Object> argsCopy = transformArgs(args);
              Object result = invoke(instance, method.getName(), argsCopy);
              if (method
                  .getReturnType()
                  .getPackageName()
                  .equals(targetInterface.getPackageName())) {
                return wrap(method.getReturnType(), result);
              }
              if (result != null
                  && !ClassUtils.isAssignable(
                      result.getClass(), method.getReturnType(), /* autoboxing = */ true)) {

                Method targetMethod = findMethod(instance.getClass(), method.getName(), argsCopy);

                throw new IllegalArgumentException(
                    String.format(
                        "Expecting method %s.%s(%s) to return %s but instead it returned %s",
                        targetMethod.getDeclaringClass().getSimpleName(),
                        targetMethod.getName(),
                        Arrays.stream(targetMethod.getParameterTypes())
                            .map(Class::getSimpleName)
                            .collect(Collectors.joining(",")),
                        method.getReturnType().getSimpleName(),
                        result.getClass().getSimpleName()));
              }
              return result;
            }));
  }

  private static List<Object> transformArgs(Object[] args) {
    if (args == null) {
      return List.of();
    }
    return Arrays.stream(args)
        .map(
            arg -> {
              if (InstanceReturner.class.isAssignableFrom(arg.getClass())) {
                try {
                  Method m = arg.getClass().getMethod("__internalGetWrappedInstance");
                  return m.invoke(arg);
                } catch (ReflectiveOperationException e) {
                  throw new IllegalArgumentException(
                      "Failed to invoke internal accessor method for wrapped object");
                }
              } else {
                return arg;
              }
            })
        .collect(Collectors.toList());
  }

  private static Constructor<?> findConstructor(Class<?> clazz, List<Object> params) {
    try {
      if (params.isEmpty()) {
        Constructor<?> c = clazz.getDeclaredConstructor();
        c.setAccessible(true);
        return c;
      }

      Class[] parameterTypes = params.stream().map(Object::getClass).toArray(Class[]::new);
      for (Constructor<?> c : clazz.getDeclaredConstructors()) {
        if (ClassUtils.isAssignable(parameterTypes, c.getParameterTypes(), true)) {
          c.setAccessible(true);
          return c;
        }
      }
    } catch (NoSuchMethodException e) {
      // ignored
    }
    throw new AccessorException(
        new NoSuchMethodException(
            String.format(
                "Constructor %s(%s) not found",
                clazz.getName(), String.join(",", classNames(params)))));
  }

  private static Method findMethod(Class<?> clazz, String name, List<Object> params) {
    Class<?>[] parameterTypes = params.stream().map(Object::getClass).toArray(Class[]::new);
    Deque<Class<?>> classes = new ArrayDeque<>();
    classes.push(clazz);
    while (!classes.isEmpty()) {
      Class<?> next = classes.pop();
      for (Method m : next.getDeclaredMethods()) {
        if (m.getName().equals(name)
            && ClassUtils.isAssignable(parameterTypes, m.getParameterTypes(), true)) {
          m.setAccessible(true);
          return m;
        }
      }
      Optional.ofNullable(next.getSuperclass()).ifPresent(classes::push);
      Arrays.stream(next.getInterfaces()).forEach(classes::push);
    }
    throw new AccessorException(
        new NoSuchMethodException(
            String.format(
                "Method %s(%s) not found", clazz.getName(), String.join(",", classNames(params)))));
  }

  private static Field findField(Class<?> clazz, String name) {
    Field f = FieldUtils.getDeclaredField(clazz, name, /* forceaccess = */ true);
    if (f == null) {
      throw new AccessorException(
          new NoSuchFieldException(String.format("Field %s.%s not found", clazz.getName(), name)));
    }
    f.setAccessible(true);
    return f;
  }

  private Class<?> loadClass(String className) {
    try {
      return Accessor.class.getClassLoader().loadClass(className);
    } catch (ClassNotFoundException e) {
      throw new AccessorException(e);
    }
  }

  private static ImmutableList<Class<?>> classes(List<Object> params) {
    return params.stream().map(p -> p == null ? null : p.getClass()).collect(toImmutableList());
  }

  private static ImmutableList<String> classNames(List<Object> params) {
    return params.stream()
        .map(p -> p == null ? "(null)" : p.getClass().getSimpleName())
        .collect(toImmutableList());
  }
}

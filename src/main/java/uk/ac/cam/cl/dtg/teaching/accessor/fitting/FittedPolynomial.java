/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor.fitting;

import com.google.common.collect.ImmutableList;

class FittedPolynomial extends FittedCurve {

  private final int degree;

  private FittedPolynomial(int degree) {
    this.degree = degree;
  }

  static FittedPolynomial create(double[] x, double[] y, int degree) {
    FittedPolynomial f = new FittedPolynomial(degree);
    f.fit(x, y);
    return f;
  }

  @Override
  ImmutableList<Point> mapValues(double[] x, double[] y) {
    ImmutableList.Builder<Point> result = ImmutableList.builder();
    for (int i = 0; i < x.length; ++i) {
      double[] xcoeff = new double[degree + 1];
      double s = 1.0;
      for (int j = 0; j <= degree; ++j) {
        xcoeff[j] = s;
        s *= x[i];
      }
      result.add(new Point(xcoeff, y[i]));
    }
    return result.build();
  }

  @Override
  Complexity complexity() {
    switch (degree) {
      case 1:
        return Complexity.LINEAR;
      case 2:
        return Complexity.QUADRATIC;
      default:
        return null;
    }
  }
}

/*
 * Copyright © 2019 The Authors (see NOTICE file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.cam.cl.dtg.teaching.accessor.fitting;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

abstract class FittedCurve {

  private double rsq;
  private double holdOutError;
  private RealMatrix coefficients;

  static class Point {
    double yvalue;
    double[] xcoeff;

    Point(double[] xcoeff, double yvalue) {
      this.yvalue = yvalue;
      this.xcoeff = xcoeff;
    }

    Point(double xcoeff, double yvalue) {
      this.yvalue = yvalue;
      this.xcoeff = new double[] {xcoeff};
    }
  }

  double holdOutError() {
    return holdOutError;
  }

  abstract Complexity complexity();

  abstract ImmutableList<Point> mapValues(double[] x, double[] y);

  double unmapY(double y) {
    return y;
  }

  void fit(double[] xs, double[] ys) {
    List<Point> mapped = mapValues(xs, ys);
    rsq = fit(mapped.subList(0, mapped.size() - 1));
    holdOutError = error(Iterables.getLast(mapped));
  }

  private double fit(List<Point> mapped) {
    int total = mapped.size();
    double[][] x = new double[total][];
    double[] y = new double[total];
    Iterator<Point> step = mapped.iterator();
    for (int i = 0; i < total; ++i) {
      Point next = step.next();
      x[i] = next.xcoeff;
      y[i] = next.yvalue;
    }

    OLSMultipleLinearRegression ols = new OLSMultipleLinearRegression();
    ols.setNoIntercept(true);
    ols.newSampleData(y, x);
    coefficients = MatrixUtils.createColumnRealMatrix(ols.estimateRegressionParameters());
    return mapped.stream()
        .mapToDouble(this::error)
        .average()
        .orElseThrow(IllegalArgumentException::new);
  }

  private double error(Point p) {
    double yhat = coefficients.preMultiply(p.xcoeff)[0];
    return unmapY(Math.pow(p.yvalue - yhat, 2.0));
  }
}
